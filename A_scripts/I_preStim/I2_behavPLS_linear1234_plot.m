% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools	= [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
pn.functions    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/T_tools/'; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]); % requires mysigstar_vert!
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/RainCloudPlots'))

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/I2_behavPLS_preStim.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

%% invert solution

stat.mask = stat.mask;
stat.prob = stat.prob.*-1;
result.vsc = result.vsc.*-1;
result.usc = result.usc.*-1;

%% ID set

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%% initiate figure

h = figure('units','normalized','position',[.1 .1 .7 .3]);

%% plot multivariate brainscores

% maskNaN = double(stat.mask);
% maskNaN(maskNaN==0) = NaN;

subplot(3,3,[2,5,8]);
statsPlot = [];
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(10:12,1:7,:).*stat.prob(10:12,1:7,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(44:60,8:14,:).*stat.prob(44:60,8:14,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask([53:55, 58:60],15:end,:).*stat.prob([53:55, 58:60],15:end,:),1)));
imagesc(stat.time-3,[],statsPlot,[-4 4])
line([3.5 6]-3, [8 8],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
line([3.5 6]-3, [16 16],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
set(gca,'Ydir','Normal');
frequencies = [stat.freq(1:7), NaN, stat.freq(8:14), NaN, stat.freq(15:end)];
set(gca, 'YTickLabels', round(frequencies(get(gca, 'YTick')),2));
xlabel('Time [s from stim onset]'); ylabel('Frequency [Hz]');
title({'Multivariate spectral changes'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'title'),'string','Mean BSR');

%% plot multivariate topographies

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
subplot(3,3,[3]); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(10:12);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-.5 .5]; plotData.powspctrm = squeeze(nanmean(nanmean(stat.mask(:,1:7,:).*stat.prob(:,1:7,:),2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Mean BSR');
subplot(3,3,[6]); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(44:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-.5 .5]; plotData.powspctrm = squeeze(nanmean(nanmean(stat.mask(:,10:15,:).*stat.prob(:,10:15,:),2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Mean BSR');
subplot(3,3,[9]); cla;
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label([53:55, 58:60]);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-.5 .5]; plotData.powspctrm = squeeze(nanmean(nanmean(stat.mask(:,25:end,:).*stat.prob(:,25:end,:),2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Mean BSR');

%% plot loadings of latent variable

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'234min1'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

individualBrainScores = uData{1}';

% plot correlation with xyz

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

idx_IDs = find(ismember(STSWD_summary.IDs, IDs));

subplot(3,3,[1,4,7]); cla;  hold on;

    a = individualBrainScores(:,1);
    b = zscore(squeeze(nanmean(STSWD_summary.HDDMwSess.driftEEG(idx_IDs,2:4),2)-nanmean(STSWD_summary.HDDMwSess.driftEEG(idx_IDs,1),2)));

    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', 'k', 'LineWidth', 3);

    scatter(a, b, 70, 'k','filled');
    [r1, p1] = corrcoef(a(~isnan(a)), b(~isnan(a)));

% add pupil modulation

    a = individualBrainScores(:,1);
   b = zscore(squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_IDs,2:4),2))-STSWD_summary.pupil.stimPupilRelChange(idx_IDs,1));

   a(b<-.03) = []; % remove one outlier
   b(b<-.03) = [];
    ls_3 = polyval(polyfit(a, b,1),a); ls_3 = plot(a, ls_3, 'Color', [.8 .8 .8], 'LineWidth', 3);

   scatter(a, b, 70, 'filled','MarkerFaceColor', [.8 .8 .8]);
    [r3, p3] = corrcoef(a(~isnan(a)), b(~isnan(a)));
   leg = legend([ls_1, ls_3], {['Drift 234-1: r = ', num2str(round(r1(2),2)), ' p = ', num2str(round(p1(2),4))];...
       ['Pupil 234-1: r = ', num2str(round(r3(2),2)), ' p = ', num2str(round(p3(2),4))]},...
       'location', 'SouthWest'); legend('boxoff')        
   xlabel({'Brainscore Change (234-1)'}); ylabel({'Change in y [z-score]';'(Loads 2:4 - Load1)'});
   title('Brainscore relations')
   %ylim([-4 3]); xlim([-800 1000])

title(['Brainscore relations (LV: p = ',num2str(round(result.perm_result.sprob(1),3)),')'])

set(findall(gcf,'-property','FontSize'),'FontSize',15)

%% move topoplots a bit closer

AxesHandle=findobj(h,'Type','axes');
axisPos = [];
axisPos{1} = get(AxesHandle(2),'OuterPosition'); 
axisPos{2} = get(AxesHandle(3),'OuterPosition'); 
axisPos{3} = get(AxesHandle(4),'OuterPosition'); 
set(AxesHandle(2),'OuterPosition',[axisPos{1}(1)-.1 axisPos{1}(2) axisPos{1}(3) axisPos{1}(4)]); 
set(AxesHandle(3),'OuterPosition',[axisPos{2}(1)-.1 axisPos{2}(2) axisPos{2}(3) axisPos{2}(4)]); 
set(AxesHandle(4),'OuterPosition',[axisPos{3}(1)-.1 axisPos{3}(2) axisPos{3}(3) axisPos{3}(4)]); 

cBarHandle=findobj(h,'Type','colorbar');
axisPos = [];
axisPos{1} = get(cBarHandle(1),'Position'); 
axisPos{2} = get(cBarHandle(2),'Position'); 
axisPos{3} = get(cBarHandle(3),'Position'); 
set(cBarHandle(1),'Position',[axisPos{1}(1)-.02 axisPos{1}(2) axisPos{1}(3) axisPos{1}(4)]); 
set(cBarHandle(2),'Position',[axisPos{2}(1)-.02 axisPos{2}(2) axisPos{2}(3) axisPos{2}(4)]); 
set(cBarHandle(3),'Position',[axisPos{3}(1)-.02 axisPos{3}(2) axisPos{3}(3) axisPos{3}(4)]); 

%% save Figure

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
figureName = 'E_overviewPlot_behavPLS';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

