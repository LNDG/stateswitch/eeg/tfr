%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
pn.savePath     = [pn.root, 'B_analyses/S2B_TFR_v6/B_data/'];
pn.plotFolder   = [pn.root, 'B_analyses/S2B_TFR_v6/C_figures/'];

addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

condEEG = 'dynamic';

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);

    %% load data

    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load([pn.History, IDs{id}, '_dynamic_config.mat'],'config');
    load([pn.dataIn, IDs{id}, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

    %% CSD transform
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    data.TrlInfo = TrlInfo; clear TrlInfo;
    data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;
    
    %% wavelets

    cfg = [];
    cfg.channel     = 'all';
    cfg.method      = 'wavelet';
    cfg.width       = 7;
    cfg.keeptrials  = 'yes';
    cfg.output      = 'pow';
    cfg.foi         = 2:1:25;
    cfg.toi         = -1.5:0.05:9.5;
    TFRwave         = ft_freqanalysis(cfg, data);
    
%     figure; imagesc(squeeze(nanmean(nanmean(log10(TFRwave.powspctrm(data.TrlInfo(:,8)==1,53:55,:,:)),2),1)))
%         figure; imagesc(squeeze(nanmean(nanmean(log10(TFRwave.powspctrm(data.TrlInfo(:,8)==4,53:55,:,:)),2),1))-...
%             squeeze(nanmean(nanmean(log10(TFRwave.powspctrm(data.TrlInfo(:,8)==2,53:55,:,:)),2),1)))
% 
%     figure; imagesc(squeeze(nanmean(nanmean(log10(TFRwave.powspctrm(data.TrlInfo(:,8)==4,53:55,:,:)),2),1)))
%     
    % add TrlInfo
    TFRwave.TrlInfo = data.TrlInfo;
    TFRwave.TrlInfoLabels = data.TrlInfoLabels;
    
    TFRwave.dimord = 'rpt_chan_freq_time';
    
    %% perform baseline correction and average within condition
    
    % 4 conditions:
    % No 1: single-trial decibel coversion to average baseline (-.8 to -.3 s)
    % No 2: single-trial log10 transform, no bl
    % No 3: single-trial decibel coversion to average baseline (-.8 to -.3 s)
    
    %% 4 load conditions
    
    timeIdx = find(TFRwave.time > -.8 & TFRwave.time < -.3);
    BaselineCorrected_singleTrial = 10*log10(TFRwave.powspctrm./repmat(nanmean(nanmean(TFRwave.powspctrm(:,:,:,timeIdx),4),1),size(TFRwave.powspctrm,1),1,1,size(TFRwave.powspctrm,4)));
    StructOut.TFRStruct_load_bl = TFRwave;
    StructOut.TFRStruct_load_bl = rmfield(StructOut.TFRStruct_load_bl, 'powspctrm');
    for indCond = 1:4
        StructOut.TFRStruct_load_bl.powspctrm(indCond,:,:,:) = squeeze(nanmean(BaselineCorrected_singleTrial(TFRwave.TrlInfo(:,8)==indCond,:,:,:),1));
    end
    
    StructOut.TFRStruct_load_log10 = TFRwave;
    StructOut.TFRStruct_load_log10 = rmfield(StructOut.TFRStruct_load_log10, 'powspctrm');
    for indCond = 1:4
        StructOut.TFRStruct_load_log10.powspctrm(indCond,:,:,:) = squeeze(nanmean(log10(TFRwave.powspctrm(TFRwave.TrlInfo(:,8)==indCond,:,:,:)),1));
    end
        
    StructOut.TFRStruct_load_avg_bl = TFRwave;
    StructOut.TFRStruct_load_avg_bl = rmfield(StructOut.TFRStruct_load_avg_bl, 'powspctrm');
    for indCond = 1:4
        timeIdx = find(TFRwave.time > -.8 & TFRwave.time < -.3);
        tmp_data = squeeze(nanmean(TFRwave.powspctrm(TFRwave.TrlInfo(:,8)==indCond,:,:,:),1));
        currentBaseline = nanmean(nanmean(TFRwave.powspctrm(:,:,:,timeIdx),4),1); % average baseline across all trials and conditions and within baseline time
        currentBaseline = repmat(currentBaseline,1,1,1,size(TFRwave.powspctrm,4));
        StructOut.TFRStruct_load_avg_bl.powspctrm(indCond,:,:,:) = 10*log10(tmp_data./squeeze(currentBaseline));
    end
    
%% save average data
        
    targetDir = [pn.savePath, 'D5_TFRavg_v6'];
    if ~exist(targetDir); mkdir(targetDir); end
    save([targetDir, '/', IDs{id}, '_', condEEG, '_TFRwavePow_v6.mat'], '-struct', 'StructOut'); clear StructOut;
    clear targetDir;
                
end % ID
