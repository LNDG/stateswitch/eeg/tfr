% Compare meancent brainscore with behavioralPLS score

%% get meancentered brainscore

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/M2_mencemtPLS_v6_wGamma.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

BS_meancent = uData{indGroup};

%% get brainscore from brahvioral PLS

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/E_behavPLS_drift.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load234min1'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

BS_behav = uData{indGroup};
% invert BS to align with plotted variant
BS_behav = -1.*BS_behav;

%% compare brainscores

h = figure
    hold on;
    a = squeeze(nanmean(BS_meancent(2:4,:),1))-BS_meancent(1,:);
    b = BS_behav(1,:);
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [.3 .5 .8], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [.3 .5 .8]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthWest'); legend('boxoff')
    xlabel('meanCent BS (234-1)'); ylabel('behavBS');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
figureName = 'G_brainscoreCorrespondence';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% get meancentered brainscore for prestim period

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/I2_mencemtPLS_v6_wGamma_preStim.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

BS_meancent_prestim = uData{indGroup};

h = figure
    hold on;
    a = squeeze(nanmean(BS_meancent(2:4,:),1))-BS_meancent(1,:);
    b = squeeze(nanmean(BS_meancent_prestim(2:4,:),1))-BS_meancent_prestim(1,:);
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [.3 .5 .8], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [.3 .5 .8]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthWest'); legend('boxoff')
    xlabel('meanCent BS (234-1)'); ylabel('BS_meancent_prestim (234-1)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

