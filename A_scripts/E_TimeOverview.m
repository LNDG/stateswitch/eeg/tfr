% visual temporal location of load effects for diverse measures

% N = 47 YAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

pn.root  = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
addpath('/Volumes/Kosciessa/Tools/barwitherr')    
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/T_tools/') % mysigstar.m

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

% extract same channels as visualized for PLS solution

idxChanTheta = 10:12;
idxChanAlpha = 44:60;
idxChanGamma = [53:55, 58:60];

%% load spectral power for theta, alpha and gamma range

    pn.WaveletRoot = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/';
    pn.MTMRoot = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/O_gammaPowBySub/';

    for indID = 1:numel(IDs)
        % load wavelet results
        tmp = load([pn.WaveletRoot, 'B_data/D5_TFRavg_v6/', IDs{indID}, '_dynamic_TFRwavePow_v6.mat'], 'TFRStruct_load_log10');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_Wavelet = find(tmp.TFRStruct_load_log10.time >= 1 & tmp.TFRStruct_load_log10.time < 8);
            TFRdata_Wavelet(indID,:,:,:,indCond) = tmp.TFRStruct_load_log10.powspctrm(indCond,:,:,timeIdx_Wavelet);
        end
        % load Gamma results
        tmp_MTM = load([pn.MTMRoot, IDs{indID}, '_GammaMTM_zscore_v2.mat'], 'Gamma');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.time >= 1 & tmp_MTM.Gamma{1,indCond}.time < 8);
            TFRdata_MTM(indID,:,:,:,indCond) = tmp_MTM.Gamma{1,indCond}.powspctrm2(:,:,timeIdx_MTM);
        end
    end
    
    TFRdata = cat(3, TFRdata_Wavelet(:,:,1:14,:,:), TFRdata_MTM(:,:,15:end,:,:));

    time = tmp.TFRStruct_load_log10.time(timeIdx_Wavelet)-3;
    freq = [tmp.TFRStruct_load_log10.freq(1:14), tmp_MTM.Gamma{1,1}.freq(15:end)];
    elec = tmp.TFRStruct_load_log10.elec;

    thetaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanTheta, freq> 2 & freq <8, :,:),3),2)),[1,3,2]);
    alphaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanAlpha, freq> 8 & freq <12, :,:),3),2)),[1,3,2]);
    gammaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanGamma, freq> 40 & freq <91, :,:),3),2)),[1,3,2]);
        
   % add within-subject error bars
    pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);
 
    %% open figure
    
%h = figure('units','normalized','position',[.1 .1 .6 .2]);
h = figure('units','normalized','position',[.1 .1 .1 .3]);

%% plot load effect: theta
    
subplot(3,1,3); 
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-6 -4];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(thetaMerged,2));
    curData = squeeze(thetaMerged(:,1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.12 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged(:,2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged(:,3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(thetaMerged(:,4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (ms); response-locked')
%     line([3 3],get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
%     line([6 6],get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
    %ll1 = legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'orientation', 'vertical', 'location', 'North', 'TextColor', [0 0 0]); set(ll1, 'color', [1 .95 .8], 'EdgeColor',  [1 .95 .8]);
        xlim([-.5 3.2]); ylim([-5.31, -5.2])
        ylabel({'2-8 Hz Theta Power';'(a.u., log10)'});
    xlabel({'Time (s)'});
    %title({'Theta power load modulation'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',13)
    
%% plot load effect: alpha

colBlue = [8.*[.1 .1 .12]; 6.*[.1 .1 .15]; 4.*[.1 .1 .2]; 2.*[.1 .1 .2]];
colGrey = [8.*[.1 .1 .1]; 6.*[.1 .1 .1]; 4.*[.1 .1 .1]; 2.*[.1 .1 .1]];
subplot(3,1,2); 
    cla; hold on;
        % highlight different experimental phases in background
        patches.timeVec = [0 3];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-6 -4];
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        % new value = old value ? subject average + grand average
        condAvg = squeeze(nanmean(alphaMerged,2));
        curData = squeeze(alphaMerged(:,1,:));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(1,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(alphaMerged(:,2,:));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(2,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(alphaMerged(:,3,:));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(3,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(alphaMerged(:,4,:));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colGrey(4,:),'linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
        xlabel('Time (ms); response-locked')
%         line([3 3],get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
%         line([6 6],get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
        ll2 = legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'orientation', 'vertical', 'location', 'North', 'TextColor', [0 0 0]); set(ll2, 'color', [1 .95 .8], 'EdgeColor',  [1 .95 .8]);
        xlim([-.5 3.2]); ylim([-5.15, -4.3])
        ylabel({'8-12 Hz Alpha Power';'(a.u., log10)'});
        xlabel({'Time (s from stimulus onset)'});
        %title({'Alpha power load modulation'; ''})
        set(findall(gcf,'-property','FontSize'),'FontSize',13)
    
   %% plot Gamma data
    
subplot(3,1,1); 
    cla; hold on;
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.05 .31];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(gammaMerged,2));
    curData = squeeze(gammaMerged(:,1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.12 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(gammaMerged(:,2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(gammaMerged(:,3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(gammaMerged(:,4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (ms); response-locked')
%     line([3 3],get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
%     line([6 6],get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
    %ll3 = legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'orientation', 'vertical', 'location', 'North', 'TextColor', [0 0 0]); set(ll3, 'color', [1 .95 .8]);set(ll3, 'EdgeColor',  [1 .95 .8]);
    xlim([-.5 3.2]); ylim([-.03 .25])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    %title({'Gamma power load modulation'; ''})

    %% save Figure
    
    set(findall(gcf,'-property','FontSize'),'FontSize',12)
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
    figureName = 'F_TimeOverview_v2';
    h.Color = 'white';
    h.InvertHardcopy = 'off';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
